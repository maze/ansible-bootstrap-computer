# Bash script for Bootstrapping a (new) Computer

## About
- This script automates many of _my_ tasks whenever starting with a fresh OS installation or new computer.
- It is tailored to _my_ use, but can act as a reference for others to adopt and adapt as their own (kind of like when sharing ones [dotfiles](https://codeberg.org/maze/dotfiles)).
- Some stages are conditional based on the user's Desktop Environment (Gnome/KDE/Xfce).

## Usage
1. Refresh sudo priveleges: `sudo true` (prevents `sudo: a password is required` error).
2. Run with ones regular user: `./bootstrap.sh` (do *not* run with `sudo`, since certain tasks rely on the `$USER` variable).
