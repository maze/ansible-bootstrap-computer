#!/usr/bin/env bash
# TODO: organize by fields (common, gnome, cyber, devops, music, etc.)

# init
set -euo
sudo true
tmp_dir=/tmp/bootstrap/$(date +%s)
mkdir -p $tmp_dir

# managed packages
apt-get update -y
apt-get upgrade -y
apt-get install -y \
  acpi \
  ansible \
  apt-transport-https \
  audacity \
  bash-completion \
  bc \
  binwalk \
  ca-certificates \
  cmake \
  containerd.io \
  cpu-x \
  curl \
  dino-im \
  dnsutils \
  docker.io \
  docker-compose-plugin \
  dos2unix \
  electrum \
  espeak \
  evtest \
  fd-find \
  ffmpeg \
  file \
  fzf \
  gimp \
  git \
  gnupg \
  gum \
  gpg \
  htop \
  jq \
  keepassxc \
  kolourpaint \
  libreoffice \
  lmms \
  mixxx \
  ncdu \
  netcat-openbsd \
  nmap \
  obs-studio \
  openssh-server \
  pandoc \
  pipx \
  preload \
  python3-pip \
  qgis \
  qpdf \
  rename \
  syncthing \
  tar \
  tcpdump \
  tealdeer \
  texlive \
  texlive-xetex \
  traceroute \
  tree \
  unzip \
  usbutils \
  vim \
  vlc \
  wget \
  whois \
  wifite \
  wireshark \
  xxd \
  xournalpp \
  zcfan \
  zip

# gnome-specific items
if [[ $XDG_SESSION_DESKTOP == 'gnome' ]]
then
  # Set alt+tab to switch windows only on current workspace
  gsettings set org.gnome.shell.app-switcher current-workspace-only true

  apt-get install -y \
    gnome-extentions \
    gnome-shell \
    gnome-tweak-tool \
    gnome-tweaks

  # install Gnome Shell extensions
  for extention in \
    workspace-indicator@gnome-shell-extensions.gcampax.github.com \
    trayIconsReloaded@selfmade.pl \
    user-theme@gnome-shell-extensions.gcampax.github.com \
    windowsNavigator@gnome-shell-extensions.gcampax.github.com \
    window-list@gnome-shell-extensions.gcampax.github.com \
    screenshot-window-sizer@gnome-shell-extensions.gcampax.github.com \
    drive-menu@gnome-shell-extensions.gcampax.github.com \
    places-menu@gnome-shell-extensions.gcampax.github.com \
    native-window-placement@gnome-shell-extensions.gcampax.github.com \
    launch-new-instance@gnome-shell-extensions.gcampax.github.com \
    auto-move-windows@gnome-shell-extensions.gcampax.github.com \
    apps-menu@gnome-shell-extensions.gcampax.github.com \
    brightnesspanelmenuindicator@do.sch.dev.gmail.com \
    vertical-overview@RensAlthuis.github.com \
    lockkeys@vaina.lt \
    clipboard-indicator@tudmotu.com \
    tophat@fflewddur.github.io \
    quick-settings-tweaks@qwreey
  do
    gnome-extentions enable $extention
  done
fi

# kde-specific items
if [[ $XDG_SESSION_DESKTOP == 'plasma' ]]
then
  kwriteconfig5 --file kscreenlockerrc --group Greeter --group LnF --group General --key showMediaControls --type bool false
fi

# xfce-specific items
if [[ $XDG_SESSION_DESKTOP == 'xfce' ]]
then
  apt-get install -y \
    thunar-archive-plugin \
    thunar-media-tags-plugin \
    xfce4-clipman-plugin \
    xfce4-cpugraph-plugin \
    xfce4-places-plugin \
    xfce4-pulseaudio-plugin \
    xfce4-systemload-plugin \
    xfce4-wavelan-plugin \
    xfce4-whiskermenu-plugin \
    xfce4-xkb-plugin
fi

# brave browser
curl -fsSL https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg -o /usr/share/keyrings/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list
apt-get update -y
apt-get install -y brave-browser

# sublime text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
apt-get update -y
apt-get install -y sublime-text

# element
wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | tee /etc/apt/sources.list.d/element-io.list
apt-get update -y
apt-get install -y element-desktop

# terraform
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
apt-get update -y
apt-get install -y terraform

# curl installations
curl -fsSL https://github.com/mikefarah/yq/releases/download/v4.18.1/yq_linux_amd64 -o /usr/local/bin/yq
chmod 0755 /usr/local/bin/yq
curl -fsSL https://dl.k8s.io/release/$(curl -Ls https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl -o /usr/bin/kubectl
chmod +x /usr/bin/kubectl
curl -fsSL https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 -o /usr/local/bin/argocd
chmod 0755 /usr/local/bin/argocd
curl -fsSL https://kind.sigs.k8s.io/dl/v0.24.0/kind-linux-amd64 -o /usr/local/bin/kind
chmod 0755 /usr/local/bin/kind
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
curl -o- https://dl-cli.pstmn.io/install/linux64.sh | sh

# zerotier vpn
curl -s https://install.zerotier.com | sudo bash
read -p "Enter the ZeroTier network ID (or leave blank to skip joining): " network_id
if [[ -n "$network_id" ]]
then
  sudo zerotier-cli join "$network_id"
fi

# burp suite
curl -fsSL 'https://portswigger-cdn.net/burp/releases/download?product=community&type=Linux' -o $tmp_dir/burpsuite_community_linux.sh
chmod +x $tmp_dir/burpsuite_community_linux.sh
echo Running Burp Suite installer... Recommended directory is /opt/
$tmp_dir/burpsuite_community_linux.sh

# aws cli
curl -fsSL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o $tmp_dir/awscli.zip
unzip $tmp_dir/awscli.zip -d $tmp_dir
$tmp_dir/aws/install --update

# manual package downloads & installations
curl -fsSL https://anydesk.com/en/downloads/thank-you?dv=deb_64 -o $tmp_dir/anydesk.deb
curl -fsSL https://github.com/sharkdp/bat/releases/download/v0.24.0/bat_0.24.0_amd64.deb -o $tmp_dir/bat.deb
curl -fsSL https://go.skype.com/skypeforlinux-64.deb -o $tmp_dir/skype.deb
curl -fsSL https://repo.steampowered.com/steam/archive/stable/steam_latest.deb -o $tmp_dir/skype.deb
curl -fsSL https://zoom.us/client/latest/zoom_amd64.deb -o $tmp_dir/zoom.deb
for package in $(ls $tmp_dir/*.deb)
do
  apt-get install ./$package
done

# groups setup
groupadd -f docker
groupadd -f wireshark
usermod -aG docker $USER
usermod -aG wireshark $USER

# python packages
pipx install --user $USER \
  boto3 \
  catt \
  colout \
  gitpython \
  pytest \
  pyyaml \
  regex \
  requests \
  termcolor \
  git+https://github.com/ytdl-org/youtube-dl  # fixes uploaded id bug, until they release the next version after 2021.12.17

# enalbe services
systemctl enable --now docker
systemctl enable --now syncthing@$USER

# cleanup
rm -rf $tmp_dir
